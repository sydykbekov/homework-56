import React from 'react';

const MenuItem = props => {
    return (
        <li>
            <span>{props.name}</span>
            <button className="push_button red" onClick={props.lessItem} disabled={props.disabled}>LESS</button>
            <button className="push_button blue" onClick={props.moreItem}>MORE</button>
        </li>
    )
};

export default MenuItem;