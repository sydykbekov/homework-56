import React from 'react';
import MenuItem from './MenuItem/MenuItem';
import './BurgerMenu.css';

const BurgerMenu = props => {

    const burgerIngredientsArray = [];
    const burgerIngredientsNames = Object.keys(props.ingredients);
    const buttonStatus = props.status;

    for (let i = 0; i < burgerIngredientsNames.length; i++) {
        const ingredientName = burgerIngredientsNames[i];

        burgerIngredientsArray.push(
            <MenuItem
                key={i}
                name={ingredientName}
                disabled={buttonStatus[ingredientName]}
                lessItem={() => props.lessItem(ingredientName)}
                moreItem={() => props.moreItem(ingredientName)}
            />
        )
    }

    return (
        <div className="menu">
            <p>Current price: {props.totalSum} soms</p>
            <ul className="list">
                {burgerIngredientsArray}
            </ul>
        </div>
    )
};

export default BurgerMenu;
